<%-- 
    Document   : CateList
    Created on : Dec 16, 2021, 9:31:55 AM
    Author     : Administrator
--%>

<%@page import="model.Category"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        ArrayList<Category> cate = (ArrayList<Category>) request.getAttribute("cList");
    %>
    <form action="cate" method="POST"> 
        CID <input type="text" name="cid"> <br>
        CName <input type="text" name="cname"> <br>
        <input type="submit" value="Insert">
    </form>
    <table border="1">
        <tr>
            <td>CID</td>
            <td>CName</td>
        </tr>
        <c:forEach items="${cList}" var="c">
            <tr>
                <td>${c.cid}</td> 
                <td>${c.cname}</td>
            </tr>
        </c:forEach>
    </table>
</html>
