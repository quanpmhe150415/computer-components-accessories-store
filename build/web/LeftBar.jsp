<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="col-sm-3">
    <div class="card bg-light mb-3">
        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
        <ul class="list-group category_block">
            <c:forEach items="${cList}" var="c">
                <li class="list-group-item text-white ${activeCid == c.cid ? "active":""}"><a href="category?cid=${c.cid}">${c.cname}</a></li>
                </c:forEach>
        </ul>
    </div>

    <div class="card bg-light mb-3">
        <div class="card-header bg-success text-white text-uppercase">Latest Product</div>
        <div class="card-body">
            <img class="img-fluid" src="${lastestP.image}">
            <h5 class="card-title">${lastestP.name}</h5>
            <p class="bloc_left_price">$${lastestP.price}</p>
        </div>
    </div>    
</div>