<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>QUANPM'S STORE</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="css/home.css" rel="stylesheet" type="text/css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            function loadMore() {
                var offset = document.getElementsByClassName("product").length;
                $.ajax({
                    url: "/Assignment/loadMore",
                    type: "get",
                    data: {
                        offset: offset
                    },
                    success: function (response) {
                        var row = document.getElementById("content");
                        row.innerHTML += response;
                    }
                });
            }

            function getProductsByString(thissearched) {
                var searched = thissearched.value;
                $.ajax({
                    url: "/Assignment/ajaxSearch",
                    type: "get",
                    data: {
                        searched: searched
                    },
                    success: function (response) {
                        var row = document.getElementById("content");
                        row.innerHTML = response;
                    }
                });
            }
        </script>
    </head>

    <body>
        <jsp:include page="Header.jsp"></jsp:include>

            <div class="container">
                <div class="row">            
                <jsp:include page="LeftBar.jsp"></jsp:include>            
                    <div class="col-sm-9">
                        <div id="content" class="row">
                        <c:forEach items="${pList}" var="p">
                            <div class="product col-12 col-md-6 col-lg-4">
                                <div class="card">
                                    <img class="card-img-top" src="${p.image}" alt="Image">
                                    <div class="card-body">
                                        <h4 class="card-title show_txt"><a href="detail?id=${p.id}" title="${p.name}">${p.name}</a></h4>
                                        <div class="row">
                                            <div class="col">
                                                <p class="btn btn-secondary btn-block">$${p.price}</p>
                                            </div>
                                            <div class="col">
                                                <a href="itemAddHome?pid=${p.id}&quantity=1" class="btn btn-success btn-block">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </c:forEach>
                    </div>
                    <button onclick="loadMore()" class="btn btn-primary">Load more</button>
                </div>
            </div>
        </div>

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>
</html>
