package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;

public class CategoryDBC extends DBContext {

    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> cList = new ArrayList<>();
        try {
            String sql = "SELECT [cid]\n"
                    + "      ,[cname]\n"
                    + "  FROM [Category]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cList.add(new Category(
                        rs.getInt(1),
                        rs.getString(2)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cList;
    }

    public Category getCategoryByCid(String cid) {
        try {
            String sql = "SELECT [cid]\n"
                    + "      ,[cname]\n"
                    + "  FROM [Category]\n"
                    + "  WHERE cid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return new Category(
                        rs.getInt(1),
                        rs.getString(2)
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertCategory(String cid, String cname) {
        try {
            String sql = "INSERT INTO [Category]\n"
                    + "           ([cid]\n"
                    + "           ,[cname])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            stm.setString(2, cname);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateCategory(String cid, String cname) {
        try {
            String sql = "UPDATE [Category]\n"
                    + "   SET [cid] = ?\n"
                    + "      ,[cname] = ?\n"
                    + " WHERE cid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(2, cname);
            stm.setString(1, cid);
            stm.setString(3, cid);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteCategory(String cid) {
        try {
            String sql = "DELETE FROM [Category]\n"
                    + "      WHERE cid = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
