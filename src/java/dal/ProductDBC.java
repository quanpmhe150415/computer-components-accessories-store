package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

public class ProductDBC extends DBContext {

    public ArrayList<Product> getAllProducts() {
        ArrayList<Product> pList = new ArrayList<>();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[image]\n"
                    + "      ,[price]\n"
                    + "      ,[description]\n"
                    + "      ,[catID]\n"
                    + "  FROM [Product]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                pList.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pList;
    }

    public ArrayList<Product> getTop6Products() {
        ArrayList<Product> top6PList = new ArrayList<>();
        try {
            String sql = "SELECT Top 6 *\n"
                    + "  FROM [Product]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                top6PList.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return top6PList;
    }

    public ArrayList<Product> getNextProducts(int offset, int amount) {
        ArrayList<Product> nextPList = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Product]\n"
                    + "  ORDER BY id\n"
                    + "  OFFSET ? ROWS\n"
                    + "  FETCH NEXT ? ROWS ONLY";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, offset);
            stm.setInt(2, amount);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                nextPList.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nextPList;
    }

    public ArrayList<Product> getProductsByCid(String catID) {
        ArrayList<Product> pListByCid = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Product]\n"
                    + "  WHERE catID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, catID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                pListByCid.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pListByCid;
    }

    public ArrayList<Product> getProductsByString(String searched) {
        ArrayList<Product> searchedPList = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Product]\n"
                    + "  WHERE name LIKE ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                searchedPList.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return searchedPList;
    }

    public Product getLatestProduct() {
        try {
            String sql = "SELECT TOP 1 *\n"
                    + "  FROM [Product]\n"
                    + "  ORDER BY id DESC";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Product getProductById(String id) {
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Product]\n"
                    + "  WHERE id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getProductAmount() {
        try {
            String sql = "SELECT COUNT (*)\n"
                    + "  FROM [Product]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<Product> productPaging(int index) {
        ArrayList<Product> pListInPage = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Product]\n"
                    + "  ORDER BY id\n"
                    + "  OFFSET ? ROWS\n"
                    + "  FETCH NEXT 5 ROWS ONLY";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, (index - 1) * 5);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                pListInPage.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6)
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pListInPage;
    }

    public void insertProduct(String name, String image, String price, String description, String catID) {
        try {
            String sql = "INSERT INTO [Product]\n"
                    + "           ([name]\n"
                    + "           ,[image]\n"
                    + "           ,[price]\n"
                    + "           ,[description]\n"
                    + "           ,[catID])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setString(2, image);
            stm.setString(3, price);
            stm.setString(4, description);
            stm.setString(5, catID);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateProduct(String id, String name, String image, String price, String description, String catID) {
        try {
            String sql = "UPDATE [Product]\n"
                    + "   SET [name] = ?\n"
                    + "      ,[image] = ?\n"
                    + "      ,[price] = ?\n"
                    + "      ,[description] = ?\n"
                    + "      ,[catID] = ?\n"
                    + " WHERE id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setString(2, image);
            stm.setString(3, price);
            stm.setString(4, description);
            stm.setString(5, catID);
            stm.setString(6, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteProduct(String id) {
        try {
            String sql = "DELETE FROM [Product]\n"
                    + "      WHERE id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
