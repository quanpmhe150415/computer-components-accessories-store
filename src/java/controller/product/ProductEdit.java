package controller.product;

import dal.CategoryDBC;
import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Product;

public class ProductEdit extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        String id = request.getParameter("id");
        ProductDBC pDB = new ProductDBC();
        CategoryDBC cDB = new CategoryDBC();
        Product p = pDB.getProductById(id);
        ArrayList<model.Category> cList = cDB.getAllCategories();     
        request.setAttribute("pDetail", p);
        request.setAttribute("cList", cList);
        request.getRequestDispatcher("ProductEdit.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
