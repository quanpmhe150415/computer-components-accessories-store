package controller.product;

import dal.CategoryDBC;
import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Product;

public class ProductManager extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pageIndex = request.getParameter("index");
        if (pageIndex == null) {
            pageIndex = "1";
        }
        int index = Integer.parseInt(pageIndex);
        ProductDBC pDB = new ProductDBC();
        ArrayList<Product> pList = pDB.productPaging(index);
        int pCount = pDB.getProductAmount();
        int pageEnd = pCount / 5;
        if (pCount % 5 != 0) {
            pageEnd++;
        }
        CategoryDBC cDB = new CategoryDBC();
        ArrayList<Category> cList = cDB.getAllCategories();
        request.setAttribute("pCount", pCount);
        request.setAttribute("pList", pList);
        request.setAttribute("cList", cList);
        request.setAttribute("pageEnd", pageEnd);
        request.setAttribute("index", index);
        request.getRequestDispatcher("ProductManager.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
