package controller;

import dal.AccountDBC;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

public class Signup extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        if (!password.equals(repassword)) {
            request.setAttribute("failmessage", "Password does not match!");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            AccountDBC aDB = new AccountDBC();
            Account a = aDB.getAccountByUsername(username);
            if (a == null) {
                aDB.insertAccount(username, password);
                request.setAttribute("successMess", "Congratulations, your account has been successfully created!");
                request.setAttribute("continueMess", "Continue to login");
                request.setAttribute("continue", "Login.jsp");
                request.getRequestDispatcher("SuccessMessage.jsp").forward(request, response);
            } else {
                request.setAttribute("failmessage", "Account already existed!");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
