package controller.category;


import dal.CategoryDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;

public class CateEdit extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cid = request.getParameter("cid");
        CategoryDBC db = new CategoryDBC();
        Category c = db.getCategoryByCid(cid);
//        ArrayList<model.Category> cList = cDB.getAllCategories();
        request.setAttribute("cDetail", c);
//        request.setAttribute("cList", cList);
        request.getRequestDispatcher("CategoryEdit.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
