package controller;

import dal.AccountDBC;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] ckArr = request.getCookies();
        if (ckArr != null) {
            for (Cookie ck : ckArr) {
                if (ck.getName().equals("c_user")) {
                    request.setAttribute("username", ck.getValue());
                }
                if (ck.getName().equals("c_pass")) {
                    request.setAttribute("password", ck.getValue());
                }
                if (ck.getName().equals("c_reme")) {
                    request.setAttribute("remember", ck.getValue());
                }
            }
        }
        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        AccountDBC aDB = new AccountDBC();
        Account a = aDB.getAccountByUP(username, password);
        if (a != null) {
            String remember = request.getParameter("remember");
            if (remember != null) {
                Cookie c_user = new Cookie("c_user", username);
                Cookie c_pass = new Cookie("c_pass", password);
                Cookie c_reme = new Cookie("c_reme", remember);
                c_user.setMaxAge(3600);
                c_pass.setMaxAge(3600);
                c_reme.setMaxAge(3600);
                response.addCookie(c_user);
                response.addCookie(c_pass);
                response.addCookie(c_reme);
            } else {
                Cookie[] ckArr = request.getCookies();
                if (ckArr != null) {
                    for (Cookie ck : ckArr) {
                        ck.setValue("");
                        ck.setPath("");
                        ck.setMaxAge(0);
                        response.addCookie(ck);
                    }
                }
            }
            HttpSession session = request.getSession();
            session.setAttribute("currentSession", a);
            request.getRequestDispatcher("home").forward(request, response);
        } else {
            request.setAttribute("failmessage", "Invalid Username or Password!");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }
    }
}
