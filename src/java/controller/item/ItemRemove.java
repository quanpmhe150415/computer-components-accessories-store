package controller.item;

import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Item;
import model.Order;
import model.Product;

public class ItemRemove extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("pid");
        ProductDBC pDB = new ProductDBC();
        Product p = pDB.getProductById(id);
        HttpSession session = request.getSession();
        Order o = (Order) session.getAttribute("order");
        ArrayList<Item> iList = o.getItems();
        boolean check = false;
        for (int i = 0; i < iList.size(); i++) {
            Item it = iList.get(i);
            if (it.getProduct().getId() == p.getId()) {
                iList.remove(it);
                check = true;
            }
        }
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        if (check == false) {
            Item i = new Item();
            i.setQuantity(quantity);
            i.setProduct(p);
            i.setPrice(i.getPrice());
            iList.add(i);
        }
        session.setAttribute("order", o);
        response.sendRedirect("cart");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
