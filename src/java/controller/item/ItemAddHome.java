package controller.item;

import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Item;
import model.Order;
import model.Product;

public class ItemAddHome extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("pid") != null) {
            String id = request.getParameter("pid");
            ProductDBC pDB = new ProductDBC();
            Product p = pDB.getProductById(id);
            Order o = new Order();
            if (p != null) {
                int quantity = Integer.parseInt(request.getParameter("quantity"));
                HttpSession session = request.getSession();
                if (session.getAttribute("order") == null) {
                    ArrayList<Item> iList = new ArrayList<>();
                    Item i = new Item(p.getId(), quantity, p.getPrice(), p);
                    iList.add(i);
                    o.setItems(iList);
                    session.setAttribute("order", o);
                } else {
                    o = (Order) session.getAttribute("order");
                    ArrayList<Item> iList = o.getItems();
                    boolean check = false;
                    for (Item i : iList) {
                        if (i.getProduct().getId() == p.getId()) {
                            i.setQuantity(i.getQuantity() + quantity);
                            check = true;
                        }
                    }
                    if (check == false) {
                        Item i = new Item(p.getId(), quantity, p.getPrice(), p);
                        iList.add(i);
                    }
                    o.setItems(iList);
                    session.setAttribute("order", o);
                }
            }
            response.sendRedirect("home");
        } else {
            response.sendRedirect("home");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
