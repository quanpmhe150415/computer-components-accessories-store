package controller;

import dal.CategoryDBC;
import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Product;

public class Home extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDBC pDB = new ProductDBC();
        ArrayList<Product> top6PList = pDB.getTop6Products();
        CategoryDBC cDB = new CategoryDBC();
        ArrayList<Category> cList = cDB.getAllCategories();
        Product lastestP = pDB.getLatestProduct();
        request.setAttribute("pList", top6PList);
        request.setAttribute("cList", cList);
        request.setAttribute("lastestP", lastestP);
        request.getRequestDispatcher("Home.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
