package controller;

import dal.CategoryDBC;
import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Product;

public class Search extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String searched = request.getParameter("searched");
        ProductDBC pDB = new ProductDBC();
        CategoryDBC cDB = new CategoryDBC();
        ArrayList<Product> searchedPList = pDB.getProductsByString(searched);
        ArrayList<model.Category> cList = cDB.getAllCategories();
        Product lastp = pDB.getLatestProduct();
        request.setAttribute("pList", searchedPList);
        request.setAttribute("cList", cList);
        request.setAttribute("latestP", lastp);
        request.setAttribute("searched", searched);
        request.getRequestDispatcher("Home.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
