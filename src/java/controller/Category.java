package controller;

import dal.CategoryDBC;
import dal.ProductDBC;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Product;

public class Category extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cid = request.getParameter("cid");
        ProductDBC pDB = new ProductDBC();
        ArrayList<Product> pListByCid = pDB.getProductsByCid(cid);
        CategoryDBC cDB = new CategoryDBC();
        ArrayList<model.Category> cList = cDB.getAllCategories();
        Product lastestP = pDB.getLatestProduct();
        request.setAttribute("activeCid", cid);
        request.setAttribute("pList", pListByCid);
        request.setAttribute("cList", cList);
        request.setAttribute("lastestP", lastestP);
        request.getRequestDispatcher("Home.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
