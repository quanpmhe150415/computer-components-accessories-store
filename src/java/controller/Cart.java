package controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Item;
import model.Order;

public class Cart extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("order") == null) {
            request.setAttribute("total", 0);
            request.setAttribute("vat", 0);
            request.setAttribute("sum", 0);
            request.getRequestDispatcher("Cart.jsp").forward(request, response);
        } else {
            Order o = (Order) session.getAttribute("order");
            ArrayList<Item> iList = o.getItems();
            int total = 0;
            for (Item it : iList) {
                total = total + it.getQuantity() * it.getPrice();
            }
            int vat = (int) (0.1 * total);
            int sum = total + vat;
            request.setAttribute("total", total);
            request.setAttribute("vat", vat);
            request.setAttribute("sum", sum);
            request.getRequestDispatcher("Cart.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
