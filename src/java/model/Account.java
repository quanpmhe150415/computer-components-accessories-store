package model;

public class Account {

    private int uid;
    private String username;
    private String password;
    private boolean isAdmin;

    public Account() {
    }

    public Account(int uid, String username, String password, boolean isAdmin) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
