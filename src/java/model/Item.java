package model;

public class Item {

    private int iid;
    private int quantity;
    private int price;
    private Product product;

    public Item() {
    }

    public Item(int iid, int quantity, int price, Product product) {
        this.iid = iid;
        this.quantity = quantity;
        this.price = price;
        this.product = product;
    }

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
