package model;

import java.util.ArrayList;

public class Order {

    private int oid;
    private ArrayList<Item> items;

    public Order() {
    }

    public Order(int oid, ArrayList<Item> items) {
        this.oid = oid;
        this.items = items;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
}
